FactoryGirl.define do
  factory :product do
    title { Faker::Commerce.product_name }
    price { Faker::Commerce.price.round(2) }
    image { Faker::Avatar.image }
    description { Faker::Hacker.say_something_smart }
  end
end
