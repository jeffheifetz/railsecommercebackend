require 'spec_helper'

describe Api::V1::ProductsController do
  describe "GET #show" do
    before(:each) do
      @product = FactoryGirl.create :product
      get :show, { id: @product.id }, {"ACCEPT" => "application/json"}
      expect(response.content_type).to eq("application/vnd.api+json")
    end


    it "returns the information about a reporter on a hash" do
      product_response = JSON.parse response.body
      expect(product_response["data"]["attributes"]["title"]).to eql @product.title
      #expect(product_response["data"]["attributes"]["price"]).to eql @product.price
      expect(product_response["data"]["attributes"]["image"]).to eql @product.image
    end

    it { should respond_with :success }
  end
end
