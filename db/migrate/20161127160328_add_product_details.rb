class AddProductDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :description, :text
    remove_column :products, :price, :money
    add_column :products, :price, :float, null: false, default: 0.0
  end
end
