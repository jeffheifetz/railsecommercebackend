class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products, id: :uuid do |t|
      t.string :title, null: false, default: ""
      t.money :price, null: false, default: 0.0
      t.string :image

      t.timestamps
    end
  end
end
