class Api::V1::ProductsController < ApplicationController

  def show
    render json: Product.find(params[:id])
  end

  def index
    products = Product.all
    render json: products
  end
end
